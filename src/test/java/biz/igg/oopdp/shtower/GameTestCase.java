/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower;
import junit.framework.TestCase;
import biz.igg.oopdp.shtower.game.Map;
import biz.igg.oopdp.shtower.game.Monster;
import biz.igg.oopdp.shtower.game.Tower;
import biz.igg.oopdp.shtower.utils.TowerAssets;
import org.newdawn.slick.SlickException;
/**
 *
 * @author salvix
 */
abstract public class GameTestCase extends BaseTestCase {
	
	protected TestGameContainer container;
	
	@Override
	public void setUp()
	{
		container = TestGame.mainSimulator();
		System.out.println("HERE");
	}
	
	@Override
	public void tearDown()
	{
		System.out.println("THERE");
		container.destroy();
	}
	
	public TestGameContainer c()
	{
		return container;
	}
	
	public static Map makeMap() throws SlickException
	{
		Map map = new Map("pack_001_map_001.tmx");
		return map;
	}
	
	public static Tower makeTower() throws SlickException
	{
		Tower t = new Tower(TowerAssets.RED, TowerAssets.LEVEL_1);
		return t;
	}
	
	public static Monster makeMonster()
	{
		Monster m = new Monster(biz.igg.oopdp.shtower.utils.MonsterAssets.VIOLET);
		return m;
	}
}
