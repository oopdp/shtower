/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower;

import biz.igg.oopdp.shtower.SimpleSingleton;
import biz.igg.oopdp.shtower.utils.TilePoint;

/**
 *
 * @author salvix
 */
public class SimpleSingletonTest extends BaseTestCase {
	
	public void testTheTest()
	{
		assertTrue(true);
	}
	
	public void testGetInstance()
	{
		SimpleSingleton s = SimpleSingleton.getInstance();
		SimpleSingleton t = SimpleSingleton.getInstance();
		assertEquals(s, t);
//		System.out.println(s.getMap());
//		SimpleSingleton.addValue("level1", "11");
//		SimpleSingleton.addValue("level2", new TilePoint(0, 0, "Something else"));
	}
	
	
	public void testAddValue()
	{
		SimpleSingleton.addValue("level1", "11");
		SimpleSingleton.addValue("level2", new TilePoint(0, 0, "Something else"));
		assertEquals(2, SimpleSingleton.count());
	}
	
	
	public void testGetValue()
	{
		TilePoint t = new TilePoint(0, 0, "Something else");
		SimpleSingleton.addValue("level1", "11");
		SimpleSingleton.addValue("level2", t);
		assertEquals(2, SimpleSingleton.count());
		assertEquals("11", SimpleSingleton.getValue("level1"));
		assertEquals(t, SimpleSingleton.getValue("level2"));
	}
}
