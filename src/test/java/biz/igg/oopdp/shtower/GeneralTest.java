package biz.igg.oopdp.shtower;

import biz.igg.oopdp.shtower.game.Map;
import biz.igg.oopdp.shtower.game.Monster;
import biz.igg.oopdp.shtower.game.Tower;
import biz.igg.oopdp.shtower.utils.TowerAssets;
import org.newdawn.slick.SlickException;

/**
 * Unit test for simple App.
 */
public class GeneralTest extends GameTestCase {


    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertFalse(false);
    }

	
    public void testAgain() throws SlickException
    {
      	Map map = new Map("pack_001_map_001.tmx");
		map.init();
		map.setCenteredWithin(1024, 768);
		assertEquals(20, map.getWidthInTiles());
		assertEquals(15, map.getHeightInTiles());
		assertEquals(192f, map.getX());
		assertEquals(144f, map.getY());
		
    }
	
	public void testAgain2() throws SlickException
    {
		
		Map map = new Map("pack_001_map_001.tmx");
		map.init();
		map.setCenteredWithin(1024, 768);
		
		Tower t = new Tower(TowerAssets.YELLOW, TowerAssets.LEVEL_1);
		t.init();
		t.setX(400);
		t.setY(380);
		
		Monster m = new Monster(biz.igg.oopdp.shtower.utils.MonsterAssets.BLUE);
		m.init();
		m.setX(300);
		m.setY(270);
		
		boolean range = t.isInRange(m);
		assertFalse(range);
		
		m.setX(420);
		m.setY(400);
		
		range = t.isInRange(m);
		assertTrue(range);
    }
	
}
