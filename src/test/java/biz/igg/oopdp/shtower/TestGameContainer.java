/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Game;
import org.newdawn.slick.SlickException;

/**
 *
 * @author salvix
 */
public class TestGameContainer extends AppGameContainer {
	
	/**
	 * 
	 * @param game
	 * @throws SlickException 
	 */
	public TestGameContainer(Game game) throws SlickException {
		super(game);
	}
	
	/**
	 * 
	 * @throws SlickException 
	 */
	public void init() throws SlickException
	{
		super.setup();
	}
	
	/**
	 * 
	 * @throws SlickException 
	 */
	public void loop() throws SlickException
	{
		
		getDelta();
		while (running()) {
			gameLoop();
		}
	}
	
	
}
