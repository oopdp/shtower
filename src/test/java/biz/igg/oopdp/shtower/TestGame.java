/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

/**
 *
 * @author salvix
 */
public class TestGame extends BasicGame
{
	
	public static final int W = 1024;
	public static final int H = 768;
	
    public TestGame()
    {
        super("Wizard game");
    }
 
    public static TestGameContainer mainSimulator()
    {
		TestGameContainer app = null;
        try
        {
            app = new TestGameContainer(new TestGame());
            app.setDisplayMode(W, H, false);
			app.init();
        }
        catch (SlickException e)
        {
			
        }
		return app;
    }
 
    @Override
    public void init(GameContainer container) throws SlickException
    {
		
    }
 
    @Override
    public void update(GameContainer container, int delta) throws SlickException
    {
		
    }
	
	@Override
    public void render(GameContainer container, Graphics g) throws SlickException
    {
		
    }

	
}
