/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils.graphs;

import biz.igg.oopdp.shtower.utils.graphs.Edge;
import biz.igg.oopdp.shtower.utils.graphs.Vertex;
import biz.igg.oopdp.shtower.BaseTestCase;

/**
 *
 * @author salvix
 */
public class EdgeTest extends BaseTestCase {
	
	public void testTheTest()
	{
		assertTrue(true);
	}
	
	public void testEdge()
	{
		String s1 = "t1";
		String s2 = "t2";
		Vertex<String> v1 = new Vertex<String>(s1);
		Vertex<String> v2 = new Vertex<String>(s2);
		Vertex<String> v3 = new Vertex<String>("t3");
		Vertex<String> v4 = new Vertex<String>("t4");
		Edge<String, String> e1 = new Edge<String, String>(v1, v2, "SSS", 10);
		assertEquals("(t1)-|10|->(t2):SSS", e1.toString());
		Edge<String, String> e2 = new Edge<String, String>(v3, v4, "BBB", 10);
		assertEquals("(t3)-|10|->(t4):BBB", e2.toString());
	}
}
