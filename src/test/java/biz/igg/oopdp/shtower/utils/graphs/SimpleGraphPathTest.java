/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils.graphs;

import biz.igg.oopdp.shtower.utils.graphs.Edge;
import biz.igg.oopdp.shtower.utils.graphs.Graph;
import biz.igg.oopdp.shtower.utils.graphs.SimpleGraphPath;
import biz.igg.oopdp.shtower.utils.graphs.Vertex;
import java.util.ArrayList;
import biz.igg.oopdp.shtower.BaseTestCase;
import biz.igg.oopdp.shtower.utils.Helper;
import static biz.igg.oopdp.shtower.utils.Helper.map2DMatrixValueForMap;
import biz.igg.oopdp.shtower.utils.TilePoint;

/**
 *
 * @author salvix
 */
public class SimpleGraphPathTest extends BaseTestCase {
	
	public void testFindShortestPath()
	{
		short[][] m = new short[][]{
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
			{ 0, 1, 2, 1, 1, 1, 1, 1, 1, 0 },
			{ 0, 1, 1, 1, 1, 1, 1, 3, 1, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
		};
		
		Vertex prev;
		Helper.print2DMatrix(m);
		System.out.println();
		Graph<String, TilePoint> g = Helper.buildFrom2DMatrix(m);
		TilePoint s = new TilePoint(2, 2, map2DMatrixValueForMap(m[2][2]));
		Vertex sv = g.findVertex(s);
		TilePoint d = new TilePoint(3, 7, map2DMatrixValueForMap(m[3][7]));
		Vertex dv = g.findVertex(d);
		
		ArrayList<Edge<String, TilePoint>> sp = SimpleGraphPath.findShortestPath(g, sv, dv);
		g.printEdges(sp);
		System.out.println();
		System.out.println();
		ArrayList<Edge<String, TilePoint>> exp = new ArrayList<Edge<String, TilePoint>>();
		TilePoint t = new TilePoint(3, 2, map2DMatrixValueForMap(m[3][2]));
		Vertex v = g.findVertex(t);
		exp.add(g.getEdge(sv, v));
		
		t = new TilePoint(3, 3, map2DMatrixValueForMap(m[3][3]));
		prev = v;
		v = g.findVertex(t);
		exp.add(g.getEdge(prev, v));
		
		
		t = new TilePoint(3, 4, map2DMatrixValueForMap(m[3][4]));
		prev = v;
		v = g.findVertex(t);
		exp.add(g.getEdge(prev, v));
		
		t = new TilePoint(3, 5, map2DMatrixValueForMap(m[3][5]));
		prev = v;
		v = g.findVertex(t);
		exp.add(g.getEdge(prev, v));
		
		t = new TilePoint(3, 6, map2DMatrixValueForMap(m[3][6]));
		prev = v;
		v = g.findVertex(t);
		exp.add(g.getEdge(prev, v));
		
		exp.add(g.getEdge(v, dv));
		
		g.printEdges(exp);
		
		assertEquals(exp, sp);
	}
	
	
	
}
