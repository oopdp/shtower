/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils.graphs;
import biz.igg.oopdp.shtower.utils.graphs.Graph;
import biz.igg.oopdp.shtower.utils.graphs.Edge;
import biz.igg.oopdp.shtower.utils.graphs.Vertex;
import java.util.ArrayList;
import biz.igg.oopdp.shtower.BaseTestCase;
import biz.igg.oopdp.shtower.utils.Helper;
import static biz.igg.oopdp.shtower.utils.Helper.map2DMatrixValueForMap;
import biz.igg.oopdp.shtower.utils.TilePoint;

/**
 *
 * @author salvix
 */
public class GraphTest extends BaseTestCase {
	
	
	public void testTheTest()
	{
		assertTrue(true);
	}
	
	public void testGraph()
	{
		Vertex<String> v1 = new Vertex<String>("1");
		Vertex<String> v2 = new Vertex<String>("2");
		Vertex<String> v3 = new Vertex<String>("3");
		Vertex<String> v4 = new Vertex<String>("4");
		Edge<String, String> e1 = new Edge<String, String>(v1, v2, "SSS", 10);
		assertEquals("(1)-|10|->(2):SSS", e1.toString());
		Edge<String, String> e2 = new Edge<String, String>(v3, v4, "BBB", 10);
		assertEquals("(3)-|10|->(4):BBB", e2.toString());
		Graph<String, String> g = new Graph<String, String>();
		g.insertVertex(v1);
		g.insertVertex(v2);
		g.insertVertex(v3);
		g.insertVertex(v4);
		assertEquals(4, g.numVertices());
		assertEquals(0, g.numEdges());
		g.insertEdge(e1);
		assertEquals(1, g.numEdges());
		g.insertEdge(e2);
		assertEquals(2, g.numEdges());
	}
	
	public void testIncomingEdges()
	{
		Vertex<String> v1 = new Vertex<String>("1");
		Vertex<String> v2 = new Vertex<String>("2");
		Vertex<String> v3 = new Vertex<String>("3");
		Vertex<String> v4 = new Vertex<String>("4");
		Edge<String, String> e1 = new Edge<String, String>(v1, v2, "SSS", 10);
		assertEquals("(1)-|10|->(2):SSS", e1.toString());
		Edge<String, String> e2 = new Edge<String, String>(v3, v4, "BBB", 10);
		assertEquals("(3)-|10|->(4):BBB", e2.toString());
		Graph<String, String> g = new Graph<String, String>();
		g.insertVertex(v1);
		g.insertVertex(v2);
		g.insertVertex(v3);
		g.insertVertex(v4);
		g.insertEdge(e1);
		g.insertEdge(e2);
		
		Vertex<String> vv4 = new Vertex<String>("4");
		Vertex<String> v5 = new Vertex<String>("5");
		
		assertEquals(v3, g.findVertex(v3));
		assertEquals(null, g.findVertex(vv4));
		assertEquals(null, g.findVertex(v5));
		
		assertEquals(v4, g.findVertex("4"));
		assertEquals(v4, g.findVertex(vv4.getElement()));
		assertEquals(null, g.findVertex(v5));
		
		
		ArrayList<Edge<String, String>> ie1 = g.incomingEdges(v1);
		assertEquals(0, ie1.size());
		ArrayList<Edge<String, String>> oe1 = g.outgoingEdges(v1);
		assertEquals(1, oe1.size());
		
		ArrayList<Edge<String, String>> ie2 = g.incomingEdges(v2);
		assertEquals(1, ie2.size());
		ArrayList<Edge<String, String>> oe2 = g.outgoingEdges(v2);
		assertEquals(0, oe2.size());
		
	}
	
	public void testGetEdge()
	{
		
		Vertex<String> v1 = new Vertex<String>("1");
		Vertex<String> v2 = new Vertex<String>("2");
		Vertex<String> v3 = new Vertex<String>("3");
		Vertex<String> v4 = new Vertex<String>("4");
		Edge<String, String> e1 = new Edge<String, String>(v1, v2, "SSS", 10);
		assertEquals("(1)-|10|->(2):SSS", e1.toString());
		Edge<String, String> e2 = new Edge<String, String>(v3, v4, "BBB", 10);
		assertEquals("(3)-|10|->(4):BBB", e2.toString());
		Graph<String, String> g = new Graph<String, String>();
		g.insertVertex(v1);
		g.insertVertex(v2);
		g.insertVertex(v3);
		g.insertVertex(v4);
		g.insertEdge(e1);
		g.insertEdge(e2);
		
		
		assertEquals(e1, g.getEdge(v1, v2));
		assertEquals(e2, g.getEdge(v3, v4));
		assertEquals(null, g.getEdge(v2, v3));
	}
	
	public void testGraphFromMap()
	{
		// 0: Blocked
		// 1: Valid
		// 2: Source
		// 3: Destination
		short[][] m = new short[][]{
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
			{ 0, 1, 2, 1, 1, 1, 1, 1, 1, 0 },
			{ 0, 1, 1, 1, 1, 1, 1, 3, 1, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
		};
		Helper.print2DMatrix(m);
		Graph<String, TilePoint> g = Helper.buildFrom2DMatrix(m);
		
		g.printVertices(g.vertices());
		g.printEdges(g.edges());
		TilePoint p = new TilePoint(4, 9, map2DMatrixValueForMap(m[4][9]));
		Vertex v = g.findVertex(p);
		assertEquals(0, g.inDegree(v));
		assertEquals(0, g.outDegree(v));
		
		p = new TilePoint(2, 4, map2DMatrixValueForMap(m[2][4]));
		v = g.findVertex(p);
		assertEquals(4, g.inDegree(v));
		assertEquals(4, g.outDegree(v));
		
		
		p = new TilePoint(3, 7, map2DMatrixValueForMap(m[3][7]));
		v = g.findVertex(p);
		assertEquals(3, g.inDegree(v));
		assertEquals(0, g.outDegree(v));
		
		p = new TilePoint(1, 1, map2DMatrixValueForMap(m[1][1]));
		v = g.findVertex(p);
		assertEquals(2, g.inDegree(v));
		assertEquals(2, g.outDegree(v));
		
		p = new TilePoint(2, 2, map2DMatrixValueForMap(m[2][2]));
		v = g.findVertex(p);
		assertEquals(4, g.inDegree(v));
		assertEquals(4, g.outDegree(v));
	}
}
