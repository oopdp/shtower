/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils;

/**
 *
 * @author salvix
 */
public class TilePoint {
	
	public float x = 0.0f;
	public float y = 0.0f;
	public Object o = null;
	
	public TilePoint(float x, float y, Object o)
	{
		this.x = x;
		this.y = y;
		this.o = o;
	}
	
	@Override
	public String toString()
	{
		return "["+x+","+y+"]:"+o;
	}
	
	public double distance(TilePoint p)
	{
		return Math.sqrt(Math.pow(x - p.x, 2) + Math.pow(y - p.y, 2));
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (!(obj instanceof TilePoint))
            return false;
        if (obj == this)
            return true;
		TilePoint p = (TilePoint) obj;
		return (p.x == x && p.y == y && p.o == o);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 71 * hash + Float.floatToIntBits(this.x);
		hash = 71 * hash + Float.floatToIntBits(this.y);
		hash = 71 * hash + (this.o != null ? this.o.hashCode() : 0);
		return hash;
	}
	
	
}
