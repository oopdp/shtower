/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils;

import java.util.HashMap;
import static biz.igg.oopdp.shtower.Game.ASSETS_PATH;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 *
 * @author salvix
 */
public class MonsterAssets {
	
	/**
	 * 
	 */
	public final static char RED = 'R';
	public final static char BLUE = 'B';
	public final static char VIOLET = 'V';
	public final static char ORANGE = 'O';
	public final static char YELLOW = 'Y';
	public final static char TURQUOISE = 'T';
	public final static char GRAY = 'A';
	public final static char GREEN = 'G';
	
	/**
	 * 
	 */
	public final static char NORTH = 'N';
	public final static char SOUTH = 'S';
	public final static char WEST = 'W';
	public final static char EAST = 'E';
	
	private static final String ASSET_FILE = "monsters.png";
	
	
	public static HashMap<Character, Integer> monstersColourMap()
	{
		HashMap<Character, Integer> colourMap = new HashMap<Character, Integer>();
		colourMap.put('R', 0);
		colourMap.put('B', 1);
		colourMap.put('V', 2);
		colourMap.put('O', 3);
		colourMap.put('Y', 4);
		colourMap.put('T', 5);
		colourMap.put('A', 6);
		colourMap.put('G', 7);
		return colourMap;
	}
	
	public static HashMap<Character, Integer> monstersDirectionMap()
	{
		HashMap<Character, Integer> dMap = new HashMap<Character, Integer>();
		dMap.put('S', 0);
		dMap.put('W', 1);
		dMap.put('E', 2);
		dMap.put('N', 3);
		return dMap;
	}
	
	public static int[] monstersColourMap(char colour)
	{
		int sizeX = 4;
		
		int[] coord = new int[2];
		HashMap<Character, Integer> colourMap = monstersColourMap();
		int position = colourMap.get(colour);
		int mod = (position % sizeX);
		coord[0] = mod;
		coord[1] = (position - mod) / sizeX;
		return coord;
	}
	
	
	
	public static int monstersDirectionMap(char direction)
	{
		HashMap<Character, Integer> directionMap = monstersDirectionMap();
		return directionMap.get(direction);
	}
	
	
	public static int[] calculateMonstersSpriteBoundaries(char direction, char colour)
	{
		int sizeX = 3;
		int sizeY = 4;
		int baseX, baseY;
		
		int[] coord = new int[4];
		// this is sloppy...need to describe sprites decentely
		int newX1, newX2, newY1, newY2;
		
		int[] colourOffset = monstersColourMap(colour);
		int directionOffset = monstersDirectionMap(direction);
		
		// colour offset:
		baseX = colourOffset[0] * sizeX;
		baseY = colourOffset[1] * sizeY;
		
		// direction offset:
		newX2 = baseX + sizeX - 1;
		newX1 = baseX;
		newY1 = baseY + directionOffset;
		newY2 = newY1;
		
		coord[0] = newX1;
		coord[1] = newY1;
		coord[2] = newX2;
		coord[3] = newY2;
		return coord;
	}
	
	public static int getTileSize()
	{
		return 32;
	}
	
	public static SpriteSheet makeMonstersSpriteSheet() throws SlickException 
	{
		return new SpriteSheet(
			ASSETS_PATH + "/" + ASSET_FILE, 
			getTileSize(), 
			getTileSize()
		);
	}
	
}
