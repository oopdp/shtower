/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils;

import biz.igg.oopdp.shtower.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author salvix
 */
public class ButtonList {
	
	int[] mouse;
	
	public int menuX;
	
	public int menuY;
	
	int menuX2;
	
	int menuY2;
	
	public int delta = 50;
	
	int charsize = 9;
	
	int padding = 10;
	
	int chosen = -1;
	
	String[] menu = {};
	
	String message;
	
	public ButtonList(String[] options)
	{
		menu = options;
	}
	
	
	public void init()
	{
		mouse = Helper.makePointArray(0, 0);
		updateMessage();
		menuX = Game.WIDTH / 2;
		menuY = Game.HEIGHT / 2;
		menuX2 = menuX;
		menuY2 = menuY;
		menuY -= (delta * menu.length) / 2;
		int longestEntry = 1;
		for(int i = 0; i < menu.length; i++){
			if(menu[i].length() > longestEntry){
				longestEntry = menu[i].length();
			}
		}
		menuX -= (charsize * longestEntry + padding);
	}
	
	public void render(GameContainer container, StateBasedGame game, Graphics g)
	{
		int maxMenuX2;
		g.drawString(message, 100, 200);
		for(int i = 0; i < menu.length; i++){
			g.drawRect(menuX, menuY + i * delta, charsize * menu[i].length() + padding * 2, padding * 4);
			g.drawString(menu[i], menuX + padding, menuY + (i * delta) + padding);
			menuY2 = menuY + (i * delta) + padding * 4;
			maxMenuX2 = menuX + charsize * menu[i].length() + padding * 2;
			if(maxMenuX2 > menuX2){
				menuX2 = maxMenuX2; 
			}
		}
	}
	
	public void mousePressed(int button, int x, int y){
		if(Input.MOUSE_LEFT_BUTTON == button){
			mouse[0] = x;
			mouse[1] = y;
			if(
				mouse[0] > this.menuX && 
				mouse[1] > this.menuY &&
				mouse[0] < this.menuX2 &&
				mouse[1] < this.menuY2
			){
				updateMessage();
			}
			else {
				message = "Click out of area";
			}
		}
	}
	
	public void updateMessage()
	{
		String button = "";
		for(int i = 0; i < menu.length; i++){
//			g.drawRect(menuX, menuY + i * delta, charsize * menu[i].length() + padding * 2, padding * 4);
			if(
				mouse[0] > menuX &&
				mouse[0] < (menuX + charsize * menu[i].length() + padding * 2) &&
				mouse[1] > menuY + (i * delta) &&
				mouse[1] < menuY + (i * delta) + padding * 4
			)
			{
				button = " on "+menu[i];
				chosen = i;
			}
		}
		message = "Click at "+Helper.fromPointToString(mouse)+button;
		
	}
	
	public int getChoice()
	{
		return chosen;
	}
	
	public void resetChoice()
	{
		chosen = -1;
	}
	
}
