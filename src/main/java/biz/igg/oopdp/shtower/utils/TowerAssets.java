/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils;

import java.util.HashMap;
import static biz.igg.oopdp.shtower.Game.ASSETS_PATH;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.util.Log;

/**
 *
 * @author salvix
 */
public class TowerAssets {
	
	public final static char RED = 'R';
	
	public final static char BLUE = 'B';
	
	public final static char VIOLET = 'V';
	
	public final static char ORANGE = 'O';
	
	public final static char YELLOW = 'Y';
	
	public final static char TURQUOISE = 'T';
	
	public final static char GRAY = 'A';
	
	public final static char GREEN = 'G';
	
	
	public final static short LEVEL_1 = 1;
	
	public final static short LEVEL_2 = 2;
	
	public final static short LEVEL_3 = 3;
	
	public final static short LEVEL_4 = 4;
	
	public final static short LEVEL_5 = 5;
	
	public final static short LEVEL_6 = 6;
	
	
	private static final String ASSET_FILE = "gems.png";
	
	private static final String BULLETS_ASSET_FILE = "bullets.png";
	
	public static HashMap<Character, Integer> colourMap()
	{
		HashMap<Character, Integer> colourMap = new HashMap<Character, Integer>();
		colourMap.put(RED, 2);
//		colourMap.put(BLUE, 1);
		colourMap.put(VIOLET, 1);
		colourMap.put(ORANGE, 3);
		colourMap.put(YELLOW, 4);
		colourMap.put(TURQUOISE, 5);
//		colourMap.put(GRAY, 6);
		colourMap.put(GREEN, 0);
		return colourMap;
	}
	
	public static HashMap<Short, Integer> levelMap()
	{
		HashMap<Short, Integer> dMap = new HashMap<Short, Integer>();
		dMap.put(LEVEL_1, 0);
		dMap.put(LEVEL_2, 4);
		dMap.put(LEVEL_3, 10);
		dMap.put(LEVEL_4, 2);
		dMap.put(LEVEL_5, 8);
		dMap.put(LEVEL_6, 6);
		return dMap;
	}
	
	public static int[] colourMap(char colour)
	{
		int sizeX = 6;
		int sizeY = 2;
		int[] coord = new int[2];
		HashMap<Character, Integer> colourMap = colourMap();
		int line = colourMap.get(colour);
		coord[0] = sizeX - 1;
		coord[1] = sizeY * line;
		return coord;
	}
	
	
	public static int[] levelMap(short level)
	{
		int sizeX = 6;
		int sizeY = 2;
		int[] coord = new int[2];
		HashMap<Short, Integer> levelMap = levelMap();
		int offset = levelMap.get(level);
		int mod = (offset % sizeX);
		coord[0] = mod;
		coord[1] = (offset - mod) / sizeX;
		return coord;
	}
	
	public static SpriteSheet makeTowersSpriteSheet() throws SlickException 
	{
		return new SpriteSheet(
			ASSETS_PATH + "/" + ASSET_FILE, 
			getTowerTileSize(), 
			getTowerTileSize()
		);
	}
	
	public static int[] calculateTowerSpriteBoundaries(char colour, short level)
	{
		int[] zero = colourMap(colour);
		int[] levelOffset = levelMap(level);
		zero[0] -= levelOffset[0];
		zero[1] += levelOffset[1];
		return zero;
	}
	
	public static int getTowerTileSize()
	{
		return 32;
	}
	
	public static int getTowerBulletTileSize()
	{
		return 8;
	}
	
	
	public static SpriteSheet makeTowerBulletsSpriteSheet() throws SlickException 
	{
		return new SpriteSheet(
			ASSETS_PATH + "/" + BULLETS_ASSET_FILE, 
			getTowerBulletTileSize(), 
			getTowerBulletTileSize()
		);
	}
	
	public static int[] towerBulletColourMap(char colour)
	{
		return colourMap(colour);
	}
	
	
	public static Sound makeSound(String source) throws SlickException
	{
		Sound s = new Sound(ASSETS_PATH + "/" + source);
		return s;
	}
	
}
