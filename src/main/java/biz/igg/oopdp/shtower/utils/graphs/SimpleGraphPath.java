/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils.graphs;

import java.util.ArrayList;
import biz.igg.oopdp.shtower.utils.TilePoint;

/**
 *
 * @author salvix
 */
public class SimpleGraphPath {
	
	public static ArrayList<Edge<String, TilePoint>> findShortestPath(Graph<String, TilePoint> g, Vertex<TilePoint> s, Vertex<TilePoint> d)
	{
		ArrayList<Edge<String, TilePoint>> pr = new ArrayList<Edge<String, TilePoint>>();
		ArrayList<Edge<String, TilePoint>> p = new ArrayList<Edge<String, TilePoint>>();
		
		// init
		int infinite = Integer.MAX_VALUE;
		int undef = Integer.MIN_VALUE;
		
		ArrayList<Vertex<TilePoint>> q = new ArrayList<Vertex<TilePoint>>();
		ArrayList<Vertex<TilePoint>> l = new ArrayList<Vertex<TilePoint>>();
		int[] dist = new int[g.numVertices()];
		int[] prev = new int[g.numVertices()];
		int[] bin = new int[g.numVertices()];
		int source = 0;
		int destination = 0;
		int count = 0;
		for (Vertex<TilePoint> v : g.vertices()) {
			q.add(v);
			l.add(v);
			if(v.equals(s)){
				source = count;
			}
			if(v.equals(d)){
				destination = count;
			}
			dist[count] = infinite;
			prev[count] = undef;
			bin[count] = 0;
			count++;
		}
		dist[source] = 0;
		
		int maxCount = 100;
		int lastBinnedId = -1;
		int vIndex = 0;
		int uIndex = 0;
		int alt = 0;
		while(!q.isEmpty()){
			uIndex = findIndexOfMinWithMask(dist, bin);
			Vertex<TilePoint> u = l.get(uIndex);
			lastBinnedId = l.indexOf(u);
//			if(lastBinnedId < 0) break;
			bin[lastBinnedId] = 1;
			q.remove(u);
			ArrayList<Edge<String, TilePoint>> outFromU = g.outgoingEdges(u);
			for(Edge<String, TilePoint> e : outFromU){
				Vertex<TilePoint> v = e.getDestination();
				vIndex = l.indexOf(v);
				if(bin[vIndex] == 0){
					alt = dist[uIndex] + e.getWeight();
					if(alt < dist[vIndex]){
						dist[vIndex] = alt;
						prev[vIndex] = uIndex;
					}
				}
			}
		}
		int curPrev = prev[destination];
		pr.add(g.getEdge(l.get(prev[destination]), l.get(destination)));
		while(curPrev != source){
			pr.add(g.getEdge(l.get(prev[curPrev]), l.get(curPrev)));
			curPrev = prev[curPrev];
			maxCount--;
			
		}
		for(int i = pr.size() - 1; i >= 0; i--){
			p.add(pr.get(i));
		}
		return p;
		
	}
	
	public static void printArray(int[] data)
	{
		for(int i = 0; i < data.length; i++){
			System.out.println(i+":"+data[i]);
		}
	}
	
	public static int findIndexOfMinWithMask(int[] data, int[] mask)
	{
		int min = Integer.MAX_VALUE;
		int minIndex = -1;
		for(int i = 0; i < data.length; i++){
			if(mask[i] == 0){
				if(data[i] <= min){
					minIndex = i;
					min = data[i];
				}
			}
		}
		return minIndex;
	}
	
	public static ArrayList<Vertex<TilePoint>> copyVerticesFromGraph(Graph<String, TilePoint> g)
	{
		ArrayList<Vertex<TilePoint>> vs = new ArrayList<Vertex<TilePoint>>();
		for (Vertex<TilePoint> v : g.vertices()) {
			vs.add(v);
		}
		return vs;
	}
}
