/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils.graphs;

/**
 * 
 * @author salvix
 * @param <E>
 * @param <V> 
 */
public class Edge<E, V> {

	private final E element;
	
    private final Vertex<V> u;

    private final Vertex<V> v;
	
	private int weight;
	
	public Edge(Vertex<V> u, Vertex<V> v, E e, int weight) {
        this.u = u;
        this.v = v;
        this.weight = weight;
		this.element = e;
    }
	
	public void setWeight(int weight)
	{
		this.weight = weight;
	}
	
	public int getWeight()
	{
		return weight;
	}
	
	@Override
	public String toString()
	{
		return u.toString()+"-|"+weight+"|->"+v.toString()+":"+element.toString();
	}
	
	public Vertex<V> getSource()
	{
		return u;
	}
	
	public Vertex<V> getDestination()
	{
		return v;
	}
	
}
