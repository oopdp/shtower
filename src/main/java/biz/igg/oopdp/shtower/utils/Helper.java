/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import biz.igg.oopdp.shtower.Game;
import biz.igg.oopdp.shtower.game.Entity;
import biz.igg.oopdp.shtower.game.Map;
import biz.igg.oopdp.shtower.game.Monster;
import biz.igg.oopdp.shtower.game.Tower;
import biz.igg.oopdp.shtower.game.manager.PlayManager;
import biz.igg.oopdp.shtower.utils.graphs.Edge;
import biz.igg.oopdp.shtower.utils.graphs.Graph;
import biz.igg.oopdp.shtower.utils.graphs.Vertex;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author salvix
 */
public class Helper {
	
	
	public static int[] makePointArray(int x, int y)
	{
		int[] coord = new int[2];
		coord[0] = x;
		coord[1] = y;
		return coord;
	}
	
	public static float[] makePointArray(float x, float y)
	{
		float[] coord = new float[2];
		coord[0] = x;
		coord[1] = y;
		return coord;
	}
	
	public static void print(Object x)
	{
		System.out.println(x);
	}
	
	public static void printPointArray(float[] p)
	{
		print("("+p[0]+","+p[1]+")");
	}
	
	public static void printPointMatrix(float[][] m)
	{
		for(int i = 0; i < m.length; i++){
			printPointArray(m[i]);
		}
	}
	
	public static void printPointArray(int[] p)
	{
		print(fromPointToString(p));
	}
	
	public static String fromPointToString(int[] p)
	{
		return "("+p[0]+","+p[1]+")";
	}
	
	
	public static void inputHandleEscape(Input input, GameContainer container, StateBasedGame game, int delta) throws SlickException
	{
		if (input.isKeyDown(Input.KEY_ESCAPE))
		{
//			container.pause();
//			game.getCurrentState().leave(container, game);
			game.enterState(Game.MENU);
		}
	}
	
	public static void inputForMonsterHandlingOnMap(Input input, int delta, Monster m, Map map)
	{
		float move = delta * 0.1f;
		if (input.isKeyDown(Input.KEY_UP))
		{
			float[] t = m.getTargetPositionForMove(MonsterAssets.NORTH, move);
			if(map.isBlockedAtBy(t, m.getWidth(), m.getHeight())){
				
			}
			else {
				m.move(MonsterAssets.NORTH, move);
			}
		}
		else if (input.isKeyDown(Input.KEY_DOWN))
		{
			float[] t = m.getTargetPositionForMove(MonsterAssets.SOUTH, move);
			if(map.isBlockedAtBy(t, m.getWidth(), m.getHeight())){
				
			}
			else {
				m.move(MonsterAssets.SOUTH, move);
			}
		}
		else if (input.isKeyDown(Input.KEY_LEFT))
		{
			float[] t = m.getTargetPositionForMove(MonsterAssets.WEST, move);
			if(map.isBlockedAtBy(t, m.getWidth(), m.getHeight())){
				
			}
			else {
				m.move(MonsterAssets.WEST, move);
			}
		}
		else if (input.isKeyDown(Input.KEY_RIGHT))
		{
			float[] t = m.getTargetPositionForMove(MonsterAssets.EAST, move);
			if(map.isBlockedAtBy(t, m.getWidth(), m.getHeight())){
				
			}
			else {
				m.move(MonsterAssets.EAST, move);
			}
		}
	}
	
	public static void print2DMatrix(short[][] m)
	{
		for(int i = 0; i < m.length; i++){
			for(int j = 0; j < m[i].length; j++){
				System.out.print(m[i][j]);
			}
			System.out.println();
		}
	}
	
	/**
	 *
	 * 0: Blocked
	 * 1: Valid	
	 * 2: Source
	 * 3: Destination
	 * @param m
	 * @return 
	 */
	public static Graph<String, TilePoint> buildFrom2DMatrix(short[][] m)
	{
		
		Graph<String, TilePoint> g = new Graph();
		for(int i = 0; i < m.length; i++){
			for(int j = 0; j < m[i].length; j++){
				TilePoint p = new TilePoint(i, j, map2DMatrixValueForMap(m[i][j]));
				Vertex<TilePoint> v = new Vertex(p);
				g.insertVertex(v);
			}
		}
		for (Vertex<TilePoint> v : g.vertices()) {
			ArrayList<Edge<String, TilePoint>> es = findAdjacentVertices(g, v);
			for(Edge<String, TilePoint> e: es){
				g.insertEdge(e);
			}
		}
		return g;
	}
	
	public static char map2DMatrixValueForMap(short s)
	{
		if(s == 0) return 'B';
		if(s == 1) return 'V';
		if(s == 2) return 'S';
		return 'D';
	}
	
	public static ArrayList<Edge<String, TilePoint>> findAdjacentVertices(Graph<String, TilePoint> g, Vertex<TilePoint> v)
	{
		ArrayList<Edge<String, TilePoint>> el = new ArrayList<Edge<String, TilePoint>>();
		
		TilePoint ref = v.getElement();
//		System.out.println(ref);
		if(ref.o.equals('V') || ref.o.equals('S')){
//			System.out.println("*"+v);
			for(Vertex<TilePoint> u: g.vertices()){
				TilePoint cur = u.getElement();
				if(!cur.o.equals('B')){
					double distance = ref.distance(cur);
					if(distance == 1.0f){ // sloppy
						Edge<String, TilePoint> e = new Edge(v, u, "e", 1);
						el.add(e);
					}
				}
			}
		}
		return el;
	}
	
	
	
}
