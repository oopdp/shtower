/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils.graphs;

import java.util.ArrayList;

/**
 *
 * See: 
 * Data Structures and
 * Algorithms in JavaTM
 * Sixth Edition
 * 
 * Graph ADT

 * MISSING METHODS:
 * 
 * endVertices(e): Returns an array containing the two endpoint vertices of
 * edge e. If the graph is directed, the first vertex is the origin
 * and the second is the destination.
 * 
 * opposite(v, e): For edge e incident to vertex v, returns the other vertex of
 * the edge; an error occurs if e is not incident to v.
 * 
 * @author salvix
 */
public class Graph<E, V> {
	
	private ArrayList<Edge<E, V>> edges;
	
	private ArrayList<Vertex<V>> vertices;
	
	public Graph()
	{
		edges = new ArrayList<Edge<E, V>>();
		vertices = new ArrayList<Vertex<V>>();
	}
	
	/**
	 * numVertices( ): Returns the number of vertices of the graph.
	 * @return 
	 */
	public int numVertices()
	{
		return vertices.size();
	}
	
	/**
	 * vertices( ): Returns an iteration of all the vertices of the graph.
	 * @return 
	 */
	public ArrayList<Vertex<V>> vertices()
	{
		return vertices;
	}
	
	/**
	 * numEdges( ): Returns the number of edges of the graph.
	 * @return 
	 */
	public int numEdges()
	{
		return edges.size();
	}
	
	/**
	 * edges( ): Returns an iteration of all the edges of the graph.
	 * @return 
	 */
	public ArrayList<Edge<E, V>> edges()
	{
		return edges;
	}
	
	/**
	 * getEdge(u, v): Returns the edge from vertex u to vertex v, if one exists;
	 * otherwise return null. For an undirected graph, there is no
	 * difference between getEdge(u, v) and getEdge(v, u).
	 * @param u
	 * @param v
	 * @return 
	 */
	public Edge<E, V> getEdge(Vertex<V> u, Vertex<V> v)
	{
		for (Edge<E, V> e : edges) {
			if(e.getSource().equals(u) && e.getDestination().equals(v)){
				return e;
			}
		}
		return null;
	}
	
//	endVertices(e): Returns an array containing the two endpoint vertices of
//	edge e. If the graph is directed, the first vertex is the origin
//	and the second is the destination.
//	
//  opposite(v, e): For edge e incident to vertex v, returns the other vertex of
//	the edge; an error occurs if e is not incident to v.
	
	/**
	 *  outDegree(v): Returns the number of outgoing edges from vertex v.
	 * @param v
	 * @return 
	 */
	public int outDegree(Vertex<V> v)
	{
		return outgoingEdges(v).size();
	}
	
	/**
	 * inDegree(v): Returns the number of incoming edges to vertex v. For
	 * an undirected graph, this returns the same value as does
	 * outDegree(v).
	 * @param v
	 * @return 
	 */
	public int inDegree(Vertex<V> v)
	{
		return incomingEdges(v).size();
	}
	
	/**
	 * outgoingEdges(v): Returns an iteration of all outgoing edges from vertex v.
	 * @param v
	 * @return 
	 */
	public ArrayList<Edge<E, V>> outgoingEdges(Vertex<V> v)
	{
		ArrayList<Edge<E, V>> outEdges = new ArrayList<Edge<E, V>>();
		for (Edge<E, V> e : edges) {
			if(e.getSource().equals(v)){
				outEdges.add(e);
			}
		}
		return outEdges;
	}
	
	/**
	 * incomingEdges(v): Returns an iteration of all incoming edges to vertex v. For
	 * an undirected graph, this returns the same collection as
	 * does outgoingEdges(v).
	 * @param v
	 * @return 
	 */
	public ArrayList<Edge<E, V>> incomingEdges(Vertex<V> v)
	{
		ArrayList<Edge<E, V>> inEdges = new ArrayList<Edge<E, V>>();
		for (Edge<E, V> e : edges) {
			if(e.getDestination().equals(v)){
				inEdges.add(e);
			}
		}
		return inEdges;
	}
	
	/**
	 * insertVertex(x): Creates and returns a new Vertex storing element x.
	 * @param v
	 * @return 
	 */
	public Vertex<V> insertVertex(V v)
	{
		Vertex<V> u = new Vertex<V>(v);
		return insertVertex(u);
	}
	
	/**
	 * insertVertex(x): Creates and returns a new Vertex storing element x.
	 * @param v
	 * @return 
	 */
	public Vertex<V> insertVertex(Vertex<V> v)
	{
		vertices.add(v);
		return v;
	}
	
	/**
	 * insertVertex(x): Creates and returns a new Vertex storing element x.
	 * @param v
	 * @return 
	 */
	public Vertex<V> findVertex(Vertex<V> v)
	{
		for (Vertex<V> u : vertices) {
			if(u.equals(v)){
				return u;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param v
	 * @return 
	 */
	public Vertex<V> findVertex(V v)
	{
		for (Vertex<V> u : vertices) {
			if(u.getElement().equals(v)){
				return u;
			}
		}
		return null;
	}
	
	/**
	 * insertEdge(u, v, x): Creates and returns a new Edge from vertex u to vertex v,
	 * storing element x; an error occurs if there already exists an
	 * edge from u to v.
	 * @param u
	 * @param v
	 * @param e
	 * @param weight
	 * @return 
	 */
	public Edge<E, V> insertEdge(Vertex<V> u, Vertex<V> v, E e, int weight)
	{
		Edge<E, V> f = new Edge<E, V>(u, v, e, weight);
		return insertEdge(f);
	}
	
	public Edge<E, V> insertEdge(Edge<E, V> e)
	{
		edges.add(e);
		return e;
	}
	
	/**
	 * removeVertex(v): Removes vertex v and all its incident edges from the graph.
	 * @param v 
	 */
	public void removeVertex(Vertex<V> v)
	{
		
	}
	
	/**
	 * removeEdge(e): Removes edge e from the graph.
	 * @param e 
	 */
	public void removeEdge(Edge<E, V> e)
	{
		
	}
	
	/**
	 * 
	 * @param c 
	 */
	public void printEdges(ArrayList<Edge<E, V>> c)
	{
		for (Edge<E, V> e : c) {
			System.out.println(e);
		}
	}
	
	public void printVertices(ArrayList<Vertex<V>> c)
	{
		for (Vertex<V> v : c) {
			System.out.println(v);
		}
	}
	
}
