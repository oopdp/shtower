/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.utils.graphs;

/**
 *
 * @author salvix
 * @param <V>
 */
public class Vertex<V> {
	
	private final V element;
	
	public Vertex(V v){
		element = v;
	}
	
	@Override
	public String toString()
	{
		return "("+element.toString()+")";
	}
	
	public V getElement()
	{
		return element;
	}
}
