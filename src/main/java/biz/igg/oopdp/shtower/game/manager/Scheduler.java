/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.game.manager;

/**
 *
 * @author salvix
 */
public class Scheduler {
	
	private long timeElapsed = 0;
	
	private long lastEmitted = 0;
	
	private final short reload = 3000;
	
	private final short offset = 100;
	
	private int issued = 0;
	
	private final int maxIssued = 20;
	
	/**
	 * This is the only thing that would be available from the Play thread
	 * @param delta
	 * @return 
	 */
	public boolean isIssuable(int delta)
	{
		timeElapsed += delta;
		lastEmitted += delta;
		if(timeElapsed > offset){
			if(lastEmitted > reload && issued < maxIssued){
				lastEmitted = 0;
				issued++;
				return true;
			}
		}
		return false;
	}
}
