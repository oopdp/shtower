/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.game;

import biz.igg.oopdp.shtower.game.Entity;
import biz.igg.oopdp.shtower.game.manager.Life;
import biz.igg.oopdp.shtower.utils.MonsterAssets;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Vector2f;

/**
 *
 * @author salvix
 */
public class Monster extends Entity {
	
	private Animation item;
	
	private Animation west;
	private Animation east;
	private Animation north;
	private Animation south;
	
	private SpriteSheet sprite;
	
	private char direction;
	
	private char colour;
	
	private Life life;
	
	/**
	 * A simple way to handle auto waypoints...
	 */
	private float[][] waypoints;
	protected Vector2f way;
	protected int currentWaypoint = 0;
	
		
	protected boolean gone = false;
	
	public Monster(char colour)
	{
		this.colour = colour;
		this.sizeX = this.sizeY = MonsterAssets.getTileSize();
		this.life = new Life(50);
	}
	
	public void init() throws SlickException 
	{
		this.sprite = biz.igg.oopdp.shtower.utils.MonsterAssets.makeMonstersSpriteSheet();
		updateDirection('W');
	}
	
	public void setDirection(char direction)
	{
		this.direction = direction;
	}
	
	public Animation getAnimation()
	{
		return item;
	}
	
	

	
	public float getCenterX()
	{
		return x + MonsterAssets.getTileSize() / 2;
	}
	
	public float getCenterY()
	{
		return y + MonsterAssets.getTileSize() / 2;
	}
	
	
	/**
	 * Improved performance
	 * @param newDirection 
	 */
	public void updateDirection(char newDirection)
	{	
		if(newDirection == direction){
			
		}
		else {
			setDirection(newDirection);
			switch(newDirection){
				case MonsterAssets.NORTH:
					if(north == null){
						north = buildAnimation(sprite, newDirection, colour);
					}
					item = north;
				break;
				case MonsterAssets.SOUTH:
					if(south == null){
						south = buildAnimation(sprite, newDirection, colour);
					}
					item = south;
				break;
				case MonsterAssets.WEST:
					if(west == null){
						west = buildAnimation(sprite, newDirection, colour);
					}
					item = west;
				break;
				case MonsterAssets.EAST:
					if(east == null){
						east = buildAnimation(sprite, newDirection, colour);
					}
					item = east;
				break;
			}
		}
	}
	
	/**
	 * 
	 * @param sprite
	 * @param direction
	 * @param colour
	 * @return 
	 */
	public static Animation buildAnimation(SpriteSheet sprite, char direction, char colour)
	{
		boolean hScan = true;
		boolean autoUpdate = true;
		int duration = 200;
		int[] coord = biz.igg.oopdp.shtower.utils.MonsterAssets.calculateMonstersSpriteBoundaries(direction, colour);
		Animation a = new Animation(
			sprite, 
			coord[0], coord[1],
			coord[2], coord[3],
			hScan,
			duration,
			autoUpdate
		);
		return a;
	}
	
	public void move(char direction, float size)
	{
		float[] coord = getTargetPositionForMove(direction, size);
		setX(coord[0]);
		setY(coord[1]);
		updateDirection(direction);
	}
	
	public float[] getPosition()
	{
		float[] coord = new float[2];
		coord[0] = x;
		coord[1] = y;
		return coord;
	}
	
	public float[] getTargetPositionForMove(char direction, float size)
	{
		float[] coord = new float[2];
		switch(direction){
			case MonsterAssets.NORTH:
				coord[0] = x;
				coord[1] = y-size;
			break;
			case MonsterAssets.SOUTH:
				coord[0] = x;
				coord[1] = y+size;
			break;
			case MonsterAssets.WEST:
				coord[0] = x - size;
				coord[1] = y;
			break;
			case MonsterAssets.EAST:
				coord[0] = x + size;
				coord[1] = y;
			break;
		}
		return coord;
	}

	@Override
	public void render(Graphics g) throws SlickException {
		if(!gone){
			getAnimation().draw(x, y);
			g.drawRect(x, y, sizeX, sizeX);
		}
	}
	
	public void setWaypoints(float[][] wp)
	{
		waypoints = wp;
		prepareNextWayVector();
	}
	
	private void prepareNextWayVector()
	{
		float[] nwp = waypoints[currentWaypoint];
		way = new Vector2f(nwp[0] - x, nwp[1] - y);
	}

	public void update(int delta) {
		float speed = delta * 0.08f;
		float moveX = (float)(speed * Math.cos(Math.toRadians(way.getTheta())));
		float moveY = (float)(speed * Math.sin(Math.toRadians(way.getTheta())));
		setX(x + Math.min(moveX, way.getX()));
		setY(y + Math.min(moveY, way.getY()));
		updateDirection(getDirectionFromTheta(way.getTheta()));
		if(currentWaypoint < waypoints.length){
			if(isWaypointReached()){
				currentWaypoint++;
				if(currentWaypoint < waypoints.length){
					prepareNextWayVector();
				}
				else {
					gone = true;
				}
			}
		}
	}
	
	public char getDirectionFromTheta(double theta)
	{
		if(Math.abs(theta - 0) < 10){
			return MonsterAssets.EAST;
		}
		
		
		if(Math.abs(theta - 90) < 10){
			return MonsterAssets.SOUTH;
		}
		
		if(Math.abs(theta - 180) < 10){
			return MonsterAssets.WEST;
		}
		
		if(Math.abs(theta - 270) < 10){
			return MonsterAssets.NORTH;
		}
		
		return MonsterAssets.WEST;
		
	}
	
	/**
	 * 
	 * @return 
	 */
	public boolean isWaypointReached()
	{
		return (Math.abs(x - waypoints[currentWaypoint][0]) < 5) && (Math.abs(y - waypoints[currentWaypoint][1]) < 5);
	}
	
	public boolean hasArrivedToFinalWaypoint()
	{
		return gone;
	}
	
	
	
	public Life getLife()
	{
		return life;
	}
	
}
