/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.game;

import biz.igg.oopdp.shtower.utils.Helper;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

/**
 *
 * @author salvix
 */
abstract public class Entity {
	
	protected float x = 0f;
	
	protected float sizeX = 1f;
	
	protected float y = 0f;
	
	protected float sizeY = 1f;
	
	public abstract float getCenterX();
	
	public abstract float getCenterY();
	
	/**
	 * Could do with a more sophisticated shape (Rectangle seems a good approximation that is also fast!)
	 */
	protected Rectangle container;
	
	public Rectangle getContainer()
	{
		if(container == null){
			container = new Rectangle(getX(), getY(), getWidth(), getHeight());
		}
		return container;
	}
	
	public float getWidth()
	{
		return sizeX;
	}
	
	public float getHeight()
	{
		return sizeY;
	}
	
	public void setX(float x)
	{
		this.x = x;
		afterSetX();
	}
	
	public void afterSetX(){
		
	}
	
	public void setY(float y)
	{
		this.y = y;
		afterSetY();
	}
	
	public void afterSetY(){
		
	}
	
	
	public abstract void render(Graphics g) throws SlickException;
	
	public float getX()
	{
		return x;
	}
	
	public float getY()
	{
		return y;
	}
	
	public float[] getPosition()
	{
		return Helper.makePointArray(x, y);
	}
	
	

}
