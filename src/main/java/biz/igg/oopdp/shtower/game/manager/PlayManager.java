/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.game.manager;

import java.util.ArrayList;
import java.util.Random;
import biz.igg.oopdp.shtower.Game;
import biz.igg.oopdp.shtower.game.BackgroundMusic;
import biz.igg.oopdp.shtower.game.Map;
import biz.igg.oopdp.shtower.game.Monster;
import biz.igg.oopdp.shtower.game.Tower;
import biz.igg.oopdp.shtower.utils.Helper;
import biz.igg.oopdp.shtower.utils.MonsterAssets;
import biz.igg.oopdp.shtower.utils.TowerAssets;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author salvix
 */
public class PlayManager {
	
	private int world;
	
	private int level;
	
	private Image background;
	
	private LevelLoader ll;
	
	private Map map;
	
	private BackgroundMusic music;
	
	private GameContainer container; 
	
	private StateBasedGame game;
	
	private Monster mm;
	
	private Tower tt;
	
	private Scheduler scheduler;
	
	private ArrayList<Monster> monsters;
	
	private ArrayList<Tower> towers;
	
	private Life life;
	
	public PlayManager(int world, int level)
	{
		this.world = world;
		this.level = level;
		ll = new LevelLoader(world, level);
	}
	
	public boolean init() throws SlickException
	{
		background = ll.getBackgroundImage();
		map = new Map("pack_001_map_001.tmx");
		map.init();
		map.setCenteredWithin(container.getWidth(), container.getHeight());
		map.buildPath();
		
		music = ll.getBackgroundMusic();
//		music.play();
		
		
		mm = new Monster(MonsterAssets.VIOLET);
		mm.init();
		mm.setX(map.getX() + 200);
		mm.setY(map.getY() + 200);
		
		tt = new Tower(TowerAssets.ORANGE, TowerAssets.LEVEL_1);
		tt.init();
		tt.setX(map.getX() + 300);
		tt.setY(map.getY() + 300);
		
		reset();
		return false;
	}
	
	protected void reset()
	{
		monsters = new ArrayList<Monster>();
		towers = new ArrayList<Tower>();
		scheduler = ll.getScheduler();
		life = ll.getLife();
	}
	
	public boolean stateInit(GameContainer container, StateBasedGame game)
	{
		this.container = container;
		this.game = game;
		return true;
	}
	
	public boolean stateRender(GameContainer container, StateBasedGame game, Graphics g) throws SlickException
	{
		background.draw();
		map.render(g);
		mm.render(g);
		tt.render(g);
		g.drawString("Monsters # "+monsters.size(), map.getX() + map.getWidthInPixels(), map.getY());
		g.drawString("Towers # "+towers.size(), map.getX() + map.getWidthInPixels(), map.getY() + 20);
		g.drawString("Life: "+life.getLife(), map.getX() + map.getWidthInPixels(), map.getY() + 40);
		monstersRender(container, game, g);
		towersRender(container, game, g);
		return true;
	}
	
	public static Tower buildTower() throws SlickException
	{
		Tower t = new Tower(TowerAssets.ORANGE, TowerAssets.LEVEL_1);
		t.init();
		return t;
	}
	
	public static Monster buildMonster() throws SlickException
	{
		Monster nm = new Monster(MonsterAssets.RED);
		nm.init();
		return nm;
	}
	
	protected Monster makeMonster() throws SlickException
	{
		float[] sp = map.getSourcePoint();
		float[][] wp = map.getPathPoints();
		Monster nm = buildMonster();
		nm.setX(sp[0]);
		nm.setY(sp[1]);
		nm.setWaypoints(wp);
		return nm;
	}
	
	protected boolean monstersRender(GameContainer container, StateBasedGame game, Graphics g) throws SlickException
	{
		for (Monster m : monsters) {
			m.render(g);
		}
		return true;
	}
	
	protected boolean towersRender(GameContainer container, StateBasedGame game, Graphics g) throws SlickException
	{
		for (Tower t : towers) {
			t.render(g);
		}
		return true;
	}
	
	public void addMonster(Monster m)
	{
		monsters.add(m);
	}
	
	public void addTower(Tower t)
	{
		towers.add(t);
	}
	
	protected boolean monstersUpdate(GameContainer container, StateBasedGame game, int delta) throws SlickException
	{
		ArrayList<Monster> arrived = new ArrayList<Monster>();
		for (Monster m : monsters) {
			m.update(delta);
			if(m.hasArrivedToFinalWaypoint()){
				arrived.add(m);
				life.sub(m.getLife().getLife());
				if(life.isThereAnyLeft()){
					
				}
				else {
					reset();
					game.enterState(Game.MENU);
					break;
				}
			}
		}
		monsters.removeAll(arrived);
		boolean issueAnotherMonster = scheduler.isIssuable(delta);
		if(issueAnotherMonster){
			addMonster(makeMonster());
		}
		return true;
	}
	
	protected boolean towersUpdate(GameContainer container, StateBasedGame game, int delta) throws SlickException
	{
		for (Tower t : towers) {
			t.update(delta);
			for(Monster m: monsters){
				if(t.isInRange(m)){
					t.fire(m);
				}
			}
		}
		return true;
	}
	
	public boolean stateUpdate(GameContainer container, StateBasedGame game, int delta) throws SlickException
	{
		monstersUpdate(container, game, delta);
		Input input = container.getInput();
		Helper.inputForMonsterHandlingOnMap(input, delta, mm, map);
		Helper.inputHandleEscape(input, container, game, delta);
		if(tt.isInRange(mm)){
			tt.fire(mm);
		}
		tt.update(delta);
		towersUpdate(container, game, delta);
		return true;
	}
	
	public void mousePressed(int button, int x, int y){
		try {
			Tower t = PlayManager.buildTower();
			t.setX(x);
			t.setY(y);
			System.out.println("Add tower here");
			addTower(t);
		} 
		catch (SlickException ex) {

		}
	}
	
}
