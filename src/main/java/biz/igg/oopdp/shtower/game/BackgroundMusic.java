/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.game;

import static biz.igg.oopdp.shtower.Game.ASSETS_PATH;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;

/**
 *
 * @author salvix
 */
public class BackgroundMusic {
	
	private Music m;
	
	private String source;
	
	public BackgroundMusic(String source) throws SlickException 
	{
		m = new Music(ASSETS_PATH + "/" + source);
	}
	
	public void play()
	{
		m.loop(1, 0.1f);
	}
	
}
