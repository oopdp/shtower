/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.game.manager;

/**
 *
 * @author salvix
 */
public class Life {
	
	private int baselife = 0;
	
	private int life = 0;
	
	public Life(int initialLife)
	{
		baselife = initialLife;
		life = baselife;
	}
	
	public int add(int life)
	{
		this.life = preview(life);
		return this.life;
	}
	
	public int sub(int life)
	{
		return this.add((-1) * life);
	}
	
	public int preview(int life)
	{
		int prev = this.life + life;
		return prev;
	}
	
	public boolean isThereAnyLifeLeftAfter(int life)
	{
		return (preview(life) > 0);
	}
	
	public boolean isThereAnyLeft()
	{
		return (life > 0);
	}
	
	public int getLife()
	{
		return life;
	}
	
}
