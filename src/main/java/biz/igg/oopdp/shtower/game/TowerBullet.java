/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.game;

import biz.igg.oopdp.shtower.game.Entity;
import biz.igg.oopdp.shtower.utils.Helper;
import biz.igg.oopdp.shtower.utils.TowerAssets;
import static biz.igg.oopdp.shtower.utils.TowerAssets.towerBulletColourMap;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Vector2f;

/**
 * 
 * @author salvix
 */
public class TowerBullet extends Entity {
	
	private SpriteSheet sprite;
	
	private char colour;
	
	private Image item;
	
	private long timestamp = 0L;
	
	protected float xTarget = 0f;
	
	protected float yTarget = 0f;
	
	protected Vector2f direction;
	
	protected boolean fired = false;
	
	public TowerBullet(char colour)
	{
		this.colour = colour;
		this.sizeX = this.sizeY = TowerAssets.getTowerBulletTileSize();
	}
	
	public void init() throws SlickException 
	{
		this.sprite = biz.igg.oopdp.shtower.utils.TowerAssets.makeTowerBulletsSpriteSheet();
		int[] spriteCoord = towerBulletColourMap(colour);
		this.item = this.sprite.getSprite(spriteCoord[0], spriteCoord[1]);
		timestamp = System.currentTimeMillis();
	}
	
	public long getTimestamp()
	{
		return timestamp;
	}
	
	public boolean hasFired()
	{
		return fired;
	}

	
	public void setTarget(float x, float y)
	{
		xTarget = x;
		yTarget = y;
		direction = new Vector2f(xTarget - this.x, yTarget - this.y);
	}
	
	public void setTarget(Entity e)
	{
//		TODO set the center...
		setTarget(e.getCenterX(), e.getCenterY());
	}
	
	/**
	 * This is not very efficient but looks nicer... see http://slick.ninjacave.com/forum/viewtopic.php?t=3983
	 * @param speed 
	 */
	public void moveToTargetByDirectionVector(float speed)
	{
		if(!fired){
			x += speed * Math.cos(Math.toRadians(direction.getTheta()));
			y += speed * Math.sin(Math.toRadians(direction.getTheta()));
			if(isTargetReached()){
				fired = true;
			}
		}
	}
	
	/**
	 * 
	 * @return 
	 */
	public boolean isTargetReached()
	{
		return (Math.abs(x - xTarget) < 10) && (Math.abs(y - yTarget) < 10);
	}
	
	
	public float getCenterX()
	{
		return x + TowerAssets.getTowerTileSize() / 2;
	}
	
	public float getCenterY()
	{
		return y + TowerAssets.getTowerTileSize() / 2;
	}
	
	@Override
	public void render(Graphics g) throws SlickException {
		if(fired){
			item.destroy();
			return;
		}
		item.draw(x, y);
		g.drawRect(x, y, sizeX, sizeX);
	}
}
