/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.game;

import java.util.ArrayList;
import biz.igg.oopdp.shtower.game.Entity;
import biz.igg.oopdp.shtower.utils.TowerAssets;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Circle;

/**
 *
 * @author salvix
 */
public class Tower extends Entity {
	
	private char colour;
	
	private Animation item;
	
	private SpriteSheet sprite;
	
	private Circle area;
	
	private float radius;
	
	private Sound sound;
	
	private short level;
	
	private short maxBullets = 10;
	
	private short bulletReloadTimeMs = 1000;
	
	private ArrayList<TowerBullet> bullets;
	
	public Tower(char colour, short level)
	{
//		this.radius = 50;
		this.level = level;
		this.colour = colour;
		this.sizeX = TowerAssets.getTowerTileSize();
		this.sizeY = TowerAssets.getTowerTileSize();
		this.radius = sizeX * 2 * level;
		this.bullets = new ArrayList<TowerBullet>();
	}
	
	
	public void init() throws SlickException 
	{
		this.sprite = TowerAssets.makeTowersSpriteSheet();
		this.item = makeAnimation();
		this.sound = TowerAssets.makeSound("blast.ogg");
		setArea();
	}
	
	@Override
	public void afterSetX()
	{
		setArea();
	}
	
	@Override
	public void afterSetY()
	{
		setArea();
	}
	
	public void setArea()
	{
		this.area = new Circle(getCenterX(), getCenterY(), radius);
	}
	
	public Animation makeAnimation()
	{
		int duration = 300;
		int[] coord = biz.igg.oopdp.shtower.utils.TowerAssets.calculateTowerSpriteBoundaries(colour, level);
		Image[] img = new Image[4];
		img[0] = this.sprite.getSprite(coord[0], coord[1]);
		img[1] = img[0].copy();
		img[2] = img[0].copy();
		img[3] = img[0].copy();
		img[1].rotate(90);
		img[2].rotate(180);
		img[3].rotate(270);
 		Animation a = new Animation(img, duration);
		return a;
	}
	
	public Animation getAnimation()
	{
		return item;
	}
	
	public float getCenterX()
	{
		return x + TowerAssets.getTowerTileSize() / 2;
	}
	
	public float getCenterY()
	{
		return y + TowerAssets.getTowerTileSize() / 2;
	}
	
	public void render(Graphics g) throws SlickException
	{
		g.drawString("Bullets # "+bullets.size(), getCenterX() + area.radius, getY());
		getAnimation().draw(x, y);
		g.drawOval(area.getCenterX() - area.radius, area.getCenterY() - area.radius, area.radius * 2, area.radius * 2);
		for (TowerBullet tb : bullets) {
			tb.render(g);
		}
		g.drawRect(x, y, sizeX, sizeX);
	}
	
	public void update(int delta) throws SlickException {
		float move = delta * 0.2f;
		ArrayList<TowerBullet> fired = new ArrayList<TowerBullet>();
		for (TowerBullet tb : bullets) {
			tb.moveToTargetByDirectionVector(move);
			if(tb.hasFired()){
				fired.add(tb);
			}
		}
		bullets.removeAll(fired);
	}
	
	
	public boolean isInRange(Entity e)
	{
		return area.contains(e.getCenterX(), e.getCenterY());
	}
	
	public long getLastBulletTimestamp()
	{
		if(bullets.size() > 0){
			TowerBullet tb = bullets.get(bullets.size() - 1);
			return tb.getTimestamp();
		}
		return 0L;
	}
	
	public void fire(Entity e) throws SlickException
	{
		long lastBullet = getLastBulletTimestamp();
		long now = System.currentTimeMillis();
		if(bullets.size() < maxBullets && (now - lastBullet) > bulletReloadTimeMs){
			this.sound.play(1, 0.05f);
			TowerBullet tb = new TowerBullet(colour);		
			tb.setX(getCenterX());
			tb.setY(getCenterY());
			tb.init();
			tb.setTarget(e);
			bullets.add(tb);
		}
	}
	
}
