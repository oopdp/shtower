/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.game;

import java.util.ArrayList;
import static biz.igg.oopdp.shtower.Game.ASSETS_PATH;
import biz.igg.oopdp.shtower.utils.Helper;
import biz.igg.oopdp.shtower.utils.TilePoint;
import biz.igg.oopdp.shtower.utils.graphs.Edge;
import biz.igg.oopdp.shtower.utils.graphs.Graph;
import biz.igg.oopdp.shtower.utils.graphs.SimpleGraphPath;
import biz.igg.oopdp.shtower.utils.graphs.Vertex;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

/**
 *
 * @author salvix
 */
public class Map extends Entity {
	
	private TiledMap map;
	
	private final String mapFile;
	
	private Graph<String, TilePoint> graph;
	
	ArrayList<Edge<String, TilePoint>> path;
	
	public Map(String mapName)
	{
		mapFile = mapName;
	}
	
	public void init() throws SlickException
	{
		map = new TiledMap(ASSETS_PATH+"/maps/"+mapFile);
		this.sizeX = getWidthInPixels();
		this.sizeY = getHeightInPixels();
	}
	
	public void buildPath()
	{
		short[][] m = fromMapToMatrix();
		initGraph(m);
		Vertex<TilePoint> s = getSource();
		Vertex<TilePoint> d = getDestination();
		findShortestPath(s, d);
	}
	
	public void initGraph(short[][] matrix)
	{
		graph = Helper.buildFrom2DMatrix(matrix);
	}
	
	public Vertex<TilePoint> getSource()
	{
		Vertex<TilePoint> ret = null;
		for(Vertex<TilePoint> s: graph.vertices()){
			if(s.getElement().o.equals('S')){
				ret = s;
				break; // changing this you flip S
			}
		}
		return ret;
	}
	
	public Vertex<TilePoint> getDestination()
	{
		Vertex<TilePoint> ret = null;
		for(Vertex<TilePoint> d: graph.vertices()){
			if(d.getElement().o.equals('D')){
				ret = d;
//				break; // changing this you flip D
			}
		}
		return ret;
	}
	
	public void findShortestPath(Vertex<TilePoint> s, Vertex<TilePoint> d)
	{
		path = SimpleGraphPath.findShortestPath(graph, s, d);
	}
	
	public ArrayList<Edge<String, TilePoint>> getPath()
	{
		return path;
	}
	
	public short[][] fromMapToMatrix()
	{
		short[][] m = new short[getWidthInTiles()][getHeightInTiles()];
		for(int i = 0; i < m.length; i++){
			for(int j = 0; j < m[i].length; j++){
				m[i][j] = mapValueAt(i, j);
			}
		}
		return m;
	}
	
	/**
	 * 0: Blocked
	 * 1: Valid	
	 * 2: Source
	 * 3: Destination
	 * @param i
	 * @param j
	 * @return 
	 */
	public short mapValueAt(int i, int j)
	{
		int[] coord = new int[2];
		coord[0] = i;
		coord[1] = j;
		boolean s = isTileAtLayer(coord, "source");
		if(s){
			return 2;
		}
		boolean d = isTileAtLayer(coord, "target");
		if(d){
			return 3;
		}
		boolean b = isBlockedTile(coord);
		if(b){
			return 0;
		}
		return 1;
	}
	
	public Graph<String, TilePoint> getGraph()
	{
		return graph;
	}

	@Override
	public float getCenterX() {
		return x;
	}

	@Override
	public float getCenterY() {
		return y;
	}
	
	public int getWidthInPixels()
	{
		return getWidthInTiles() * map.getTileWidth();
	}
	
	public int getHeightInPixels()
	{
		return getHeightInTiles() * map.getTileHeight();
	}
	
	public int getWidthInTiles()
	{
		return map.getWidth();
	}
	
	public int getHeightInTiles()
	{
		return map.getHeight();
	}
	
	public void setCenteredWithin(int width, int height)
	{
		setX((width - getWidthInPixels())/2);
		setY((height - getHeightInPixels())/2);
	}
	
	@Override
	public void render(Graphics g) throws SlickException
	{
		map.render((int)x, (int)y);
		g.drawRect(x, y, sizeX, sizeY);
		Color tmp = g.getColor();
		g.setColor(Color.red);
		float dotX = 0;
		float dotY = 0;
		for(int i = 0; i < path.size() - 1; i++){
			TilePoint p = path.get(i).getDestination().getElement();
			dotX = p.x * map.getTileWidth() + getX() + map.getTileWidth() / 2;
			dotY = p.y * map.getTileHeight() + getY() + map.getTileHeight() / 2;
			g.drawOval(dotX, dotY, 6, 6);
		}
		g.setColor(tmp);
	}
	
	
	/**
	 * Do you want to position something relative to the top-most corner of the map?
	 * @param x
	 * @param y
	 * @return 
	 */
	public float[] getCoordinatesRelativeToMap(float x, float y) 
	{
		float[] coord = new float[2];
		coord[0] = (int) (this.x + x);
		coord[1] = (int) (this.y + y);
		return coord;
	}
	
	public int getBlockedLayer()
	{
		return map.getLayerIndex("blocked");
	}
	
	public boolean isTileAtLayer(int [] tile, String layer)
	{
		return !(map.getTileId(tile[0], tile[1], map.getLayerIndex(layer)) == 0);
	}
	
	public boolean isBlockedTile(int [] tile)
	{
		return !(map.getTileId(tile[0], tile[1], getBlockedLayer()) == 0);
	}

	public boolean isBlockedAt(float x, float y) 
	{
		int[] tile = getTileByCoordinates(x, y);
		return isBlockedTile(tile);
	}
	
	
	public boolean isBlockedAtBy(float [] coord, float x, float y) 
	{
		boolean tl = !isBlockedAt(coord[0], coord[1]);
		boolean tr = !isBlockedAt(coord[0] + x, coord[1]);
		boolean br = !isBlockedAt(coord[0], coord[1] + y);
		boolean bl = !isBlockedAt(coord[0] + x, coord[1] + y);
		boolean b = !(tl && tr && br && bl);
		return b;
	}
	
	public boolean isBlockedAtBy(float atX, float atY, float x, float y) 
	{
		float[] coord = Helper.makePointArray(atX, atY);
		return isBlockedAtBy(coord, x, y);
	}
	
	public boolean isBlockedAt(float [] coord) 
	{
		return isBlockedAt(coord[0], coord[1]);
	}
	
	public int[] getTileByCoordinates(float x, float y)
	{
		int[] coord = Helper.makePointArray(0, 0);
		int tH = map.getTileHeight();
		int tW = map.getTileWidth();
		float relX = x - this.x;
		float relY = y - this.y;
		coord[0] = (int) Math.floor(relX / tW);
		coord[1] = (int) Math.floor(relY / tH);
		return coord;
	}
	
	public float[] getSourcePoint()
	{
		float[] sP = Helper.makePointArray(0f, 0f);
		ArrayList<Edge<String, TilePoint>> p = getPath();
		TilePoint tp = p.get(0).getSource().getElement();
		sP[0] = tp.x * map.getTileWidth() + getX();
		sP[1] = tp.y * map.getTileHeight() + getY();
		return sP;
	}
	
	public float[][] getPathPoints()
	{
		ArrayList<Edge<String, TilePoint>> p = getPath();
		float[][] pPoints = new float[p.size()][2];
		for(int i = 0; i < p.size(); i++){
			TilePoint tp = p.get(i).getDestination().getElement();
			pPoints[i][0] = tp.x * map.getTileWidth() + getX();
			pPoints[i][1] = tp.y * map.getTileHeight() + getY();
		}
		return pPoints;
	}

	
}
