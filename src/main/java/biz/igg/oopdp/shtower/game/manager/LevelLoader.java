/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.game.manager;

import static biz.igg.oopdp.shtower.Game.ASSETS_PATH;
import biz.igg.oopdp.shtower.game.BackgroundMusic;
import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;

/**
 *
 * @author salvix
 */
public class LevelLoader {
	
	private int world;
	
	private int level;
	
	public LevelLoader(int world, int level)
	{
		this.world = world;
		this.level = level;
	}
	
	/**
	 * 
	 * @return
	 * @throws SlickException 
	 */
	public Image getBackgroundImage() throws SlickException
	{
		Image img = new Image(ASSETS_PATH + "/splash_bg.png");
		return img;
	}
	
	/**
	 * 
	 * @return
	 * @throws SlickException 
	 */
	public BackgroundMusic getBackgroundMusic() throws SlickException
	{
		BackgroundMusic music = new BackgroundMusic("magic.ogg");
		return music;
	}
	
	public Scheduler getScheduler() 
	{
		Scheduler s = new Scheduler();
		return s;
	}
	
	public Life getLife()
	{
		Life l = new Life(1000);
		return l;
	}
}
