/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower;

import biz.igg.oopdp.shtower.states.Exit;
import biz.igg.oopdp.shtower.states.Level;
import biz.igg.oopdp.shtower.states.Menu;
import biz.igg.oopdp.shtower.states.Play;
import biz.igg.oopdp.shtower.states.Sandbox;
import biz.igg.oopdp.shtower.states.Splash;
import biz.igg.oopdp.shtower.states.World;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author salvix
 */
public class Game extends StateBasedGame {
	
	/** Screen width */
	
    public static final int WIDTH = 1024;
    
	/** Screen height */    
	public static final int HEIGHT = 768;
	
	public static final String ASSETS_PATH = "assets";
	
	public static final String GAME_NAME = "ShTower";
	
    // Game state identifiers
    public static final int SPLASHSCREEN = 0;
    public static final int MENU = 10;
	public static final int WORLD = 20;
	public static final int LEVEL = 30;
    public static final int PLAY = 50;
	public static final int EXIT = 100;
	
	/**
	 * The magical state for code assessment.
	 */
	public static final int SANDBOX = 1000;

    // Application Properties
    public static final int FPS = 60;
    public static final double VERSION = 1.0;

    /**
	 * 
	 * @param appName 
	 */
	public Game(String appName) {
        super(appName);
    }
	
	/**
	 * 
	 * @param container
	 * @throws SlickException 
	 */
	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		
		
		this.addState(new Splash());
		this.addState(new Menu());
		this.addState(new World());
		this.addState(new Level());
		this.addState(new Play());
		this.addState(new Exit());
		/**
		 * Use this for visual experiments and comment previous..
		 */
		this.addState(new Sandbox());
	}
	
	/**
	 * 
	 * @param args
	 * @throws SlickException 
	 */
	public static void main(String[] args) throws SlickException {
        AppGameContainer app = new AppGameContainer(new Game(GAME_NAME));
        app.setDisplayMode(WIDTH, HEIGHT, false);
        app.setForceExit(false);
        app.start();
    }

	
}