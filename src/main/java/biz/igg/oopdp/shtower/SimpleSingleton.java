/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower;

import java.util.HashMap;

/**
 *
 * @author salvix
 */
public class SimpleSingleton {
	
	private static SimpleSingleton instance = null;
	
	private HashMap<String, Object> map;
	
	protected SimpleSingleton() {
	   // Exists only to defeat instantiation.
	   map = new HashMap();    
	}
	
	public static SimpleSingleton getInstance() {
		if(instance == null) {
			instance = new SimpleSingleton();
		}
		return instance;
	}
	
	public HashMap<String, Object> getMap()
	{
		return map;
	}
	
	public static void addValue(String key, Object value)
	{
		getInstance().getMap().put(key, value);
	}
	
	public static Object getValue(String key)
	{
		return getInstance().getMap().get(key);
	}
	
	public static boolean isSet(String key)
	{
		return getInstance().getMap().containsKey(key);
	}
	
	public static int count()
	{
		return getInstance().getMap().size();
	}
}
