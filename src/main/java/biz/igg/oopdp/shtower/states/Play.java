/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.states;

import biz.igg.oopdp.shtower.Game;
import biz.igg.oopdp.shtower.SimpleSingleton;
import biz.igg.oopdp.shtower.game.manager.PlayManager;
import biz.igg.oopdp.shtower.utils.Helper;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author salvix
 */
public class Play extends State {
	
	/** A counter... */
    private int counter;
	
	private int world = -1;
	
	private int level = -1;
	
	private boolean isInit = false;
	
	private PlayManager pl;
	
	@Override
	public int getID() {
		return Game.PLAY;
	}
	
	public boolean isInit()
	{
		return isInit;
	}
	
	
	public void initPlay(GameContainer container, StateBasedGame game) throws SlickException
	{
		if(SimpleSingleton.isSet("current_world")){
			world = (Integer) SimpleSingleton.getValue("current_world");
		}
		if(SimpleSingleton.isSet("current_level")){
			level = (Integer) SimpleSingleton.getValue("current_level");
		}
		if(!isInit() && world >= 0 && level >= 0){
			pl = new PlayManager(world, level);
			pl.stateInit(container, game);
			pl.init();
			isInit = true;
		}
	}
	
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		counter = 0;
	}

	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		if(isInit()){
			pl.stateRender(container, game, g);
		}
		g.drawString("Hello, " + Integer.toString(counter) + "!", 50, 50);		
		g.drawString("You are playing in World " + Integer.toString(world) + " at level " +Integer.toString(level) + "!", 50, 100);
	}

	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		initPlay(container, game);
		if(!isInit()){
			game.enterState(Game.WORLD);
			
		}
		else {
			counter++;
			Input input = container.getInput();
			pl.stateUpdate(container, game, delta);
		}
	}
	
	@Override
	public void mousePressed(int button, int x, int y){
		pl.mousePressed(button, x, y);
	}
	
}
