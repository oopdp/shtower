/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.states;

import biz.igg.oopdp.shtower.Game;
import biz.igg.oopdp.shtower.SimpleSingleton;
import biz.igg.oopdp.shtower.utils.ButtonList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author salvix
 */
public class World extends State {
	
	ButtonList bl;
	
	String [] menu = {"World 1", "World 2", "World 3"};
	
	@Override
	public int getID() {
		return Game.WORLD;
	}

	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		bl = new ButtonList(menu);
		bl.init();
	}

	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		g.drawString("WORLD?", 0, 0);
		bl.render(container, game, g);
	}

	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		int chosen = bl.getChoice();
		if(chosen >= 0){
			handleButton(chosen, container, game);
			bl.resetChoice();
		}
	}
	
	
	public void handleButton(int chosen, GameContainer container, StateBasedGame game)
	{
		SimpleSingleton.addValue("current_world", chosen);
		game.enterState(Game.LEVEL);
	}
	
	@Override
	public void mousePressed(int button, int x, int y){
		bl.mousePressed(button, x, y);
	}
	
}
