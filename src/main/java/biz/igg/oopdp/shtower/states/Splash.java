/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.states;

import biz.igg.oopdp.shtower.Game;
import static biz.igg.oopdp.shtower.Game.ASSETS_PATH;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author salvix
 */
public class Splash extends State {

	Image img;
	
	int running = 0;
	
	@Override
	public int getID() {
		return Game.SPLASHSCREEN;
	}

	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		img = new Image(ASSETS_PATH + "/splash.png");
	}

	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		img.draw();
	}

	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		running += delta;
		if(running > 1000){
			game.enterState(Game.MENU);
		}
	}
	
}
