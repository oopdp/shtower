/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.states;

import biz.igg.oopdp.shtower.Game;
import biz.igg.oopdp.shtower.utils.ButtonList;
import biz.igg.oopdp.shtower.utils.Helper;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author salvix
 */
public class Exit extends State {
	
	ButtonList bl;
	
	@Override
	public int getID() {
		return Game.EXIT;
	}

	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		String[] opts = {"Yes", "No"};
		bl = new ButtonList(opts);
		bl.init();
	}

	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		g.drawString("Do you want to quit the game?", bl.menuX - (int)(bl.delta * 2), bl.menuY - bl.delta);
		bl.render(container, game, g);
	}
	
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		int chosen = bl.getChoice();
		if(chosen >= 0){
			handleButton(chosen, container, game);
			bl.resetChoice();
		}
	}
	
	@Override
	public void mousePressed(int button, int x, int y){
		bl.mousePressed(button, x, y);
	}

	
	public void handleButton(int chosen, GameContainer container, StateBasedGame game)
	{
		switch(chosen){
			case 0:
				container.exit();
			break;
			case 1:
				game.enterState(Game.MENU);
			break;
		}
	}
	
	
}
