/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.states;

import biz.igg.oopdp.shtower.Game;
import biz.igg.oopdp.shtower.SimpleSingleton;
import biz.igg.oopdp.shtower.utils.ButtonList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author salvix
 */
public class Level extends State {
	
	ButtonList bl = null;
	
	String[][] menu = {
		{"W1: Level 1", "W1: Level 2", "W1: Level 3"},
		{"W2: Level 1", "W2: Level 2", "W2: Level 3"},
		{"W3: Level 1", "W3: Level 2", "W3: Level 3"}
	};
	
	int world = -1;
	
	@Override
	public int getID() {
		return Game.LEVEL;
	}

	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		
	}

	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		g.drawString("LEVELS for world "+world, 0, 0);
		bl.render(container, game, g);
	}

	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		initButtonList();
		int chosen = bl.getChoice();
		if(chosen >= 0){
			handleButton(chosen, container, game);
			bl.resetChoice();
		}
	}
	
	public void handleButton(int chosen, GameContainer container, StateBasedGame game)
	{
		SimpleSingleton.addValue("current_level", chosen);
		game.enterState(Game.PLAY);
	}
	
	public boolean initButtonList()
	{
		if(bl == null){
			if(SimpleSingleton.isSet("current_world")){
				world = (Integer) SimpleSingleton.getValue("current_world");
				bl = new ButtonList(menu[world]);
				bl.init();
				return true;
			}
			else {
				return false;
			}
		}
		return true;
	}
	
	
	@Override
	public void mousePressed(int button, int x, int y){
		bl.mousePressed(button, x, y);
	}
	
}
