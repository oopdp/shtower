/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.shtower.states;

import biz.igg.oopdp.shtower.Game;
import static biz.igg.oopdp.shtower.Game.ASSETS_PATH;
import biz.igg.oopdp.shtower.game.Map;
import biz.igg.oopdp.shtower.game.Monster;
import biz.igg.oopdp.shtower.game.Tower;
import biz.igg.oopdp.shtower.game.TowerBullet;
import biz.igg.oopdp.shtower.utils.Helper;
import biz.igg.oopdp.shtower.utils.MonsterAssets;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import biz.igg.oopdp.shtower.utils.TowerAssets;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.tiled.TiledMap;
/**
 *
 * @author salvix
 */
public class Sandbox extends State {
	
	private Map map;
	
	private Tower t1;
	
	private Tower t2;
	
	private Tower t3;
	
	private Tower t4;
	
	private Monster m1;
	
	private Monster m2;
	
	private Monster m3;
	
	private Monster m4;
	
	private Monster m5;
	
	private Monster m6;

	private Monster m7;
		
	
	private Monster m8;
	
	private Monster m9;
		
	private final int mapX = 192;
	
	private final int mapY = 64;
	
	private Image img;
	
	private boolean west = false;
	
//	private TowerBullet tb;
	
	
//	private 
	
	@Override
	public int getID() {
		return Game.SANDBOX;
	}

	public void init(GameContainer container, StateBasedGame game) throws SlickException {
//		img = new Image(ASSETS_PATH + "/transpRed75.png");
		
		map = new Map("pack_001_map_001.tmx");
		map.init();
		map.setCenteredWithin(container.getWidth(), container.getHeight());
		map.buildPath();
		
		
		
		t1 = new Tower(TowerAssets.RED, TowerAssets.LEVEL_4);
		t1.init();
		t1.setX(500);
		t1.setY(200);
		
		
		t2 = new Tower(TowerAssets.GREEN, TowerAssets.LEVEL_4);
		t2.init();
		t2.setX(300);
		t2.setY(400);
		
		
		t3 = new Tower(TowerAssets.YELLOW, TowerAssets.LEVEL_2);
		t3.init();
		t3.setX(500);
		t3.setY(380);
		
		
		t4 = new Tower(TowerAssets.YELLOW, TowerAssets.LEVEL_1);
		t4.init();
		t4.setX(400);
		t4.setY(380);
//		
//		tb = new TowerBullet(TowerAssets.VIOLET);
//		tb.init();
//		tb.setX(t1.getX());
//		tb.setY(t1.getY());
		float[] sp = map.getSourcePoint();
		
		m1 = new Monster(biz.igg.oopdp.shtower.utils.MonsterAssets.BLUE);
		m1.init();
		m1.setX(300);
		m1.setY(270);
		
		
		float[][] wp = map.getPathPoints();
		m2 = new Monster(biz.igg.oopdp.shtower.utils.MonsterAssets.YELLOW);
		m2.init();
		m2.setX(sp[0]);
		m2.setY(sp[1]);
		m2.setWaypoints(wp);
		
		m3 = new Monster(biz.igg.oopdp.shtower.utils.MonsterAssets.GREEN);
		m3.init();
		m3.setX(270);
		m3.setY(285);
		
		
		m4 = new Monster(biz.igg.oopdp.shtower.utils.MonsterAssets.TURQUOISE);
		m4.init();
		m4.setX(270);
		m4.setY(285);
		
		
		m5 = new Monster(biz.igg.oopdp.shtower.utils.MonsterAssets.VIOLET);
		m5.init();
		m5.setX(250);
		m5.setY(285);
	}

	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		
		map.render(g);
		t1.render(g);
		t2.render(g);
		t3.render(g);
//		tb.draw();
		m1.render(g);
		m2.render(g);
//		m3.draw();
//		m4.draw();
//		m5.draw();
//		tb.setTarget(m);
//		g.drawLine(t1.getX(), t1.getY(), m.getX(), m.getY());

//		img.draw(224, 176);
	}

	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		
		Input input = container.getInput();
		Helper.inputHandleEscape(input, container, game, delta);
		Helper.inputForMonsterHandlingOnMap(input, delta, m1, map);
		if(t1.isInRange(m1)){
			t1.fire(m1);
		}
		
		if(t2.isInRange(m1)){
			t2.fire(m1);
		}
		
		if(t3.isInRange(m1)){
			t3.fire(m1);
		}
		m2.update(delta);
		t1.update(delta);
		t2.update(delta);
		t3.update(delta);
	}
	
}
